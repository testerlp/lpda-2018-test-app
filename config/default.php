<?php

/* 
 * © Loopia. All rights reserved.
 */

$settings = [];

$settings['template.base_path'] = realpath(__DIR__ . '/../template');

$settings['paths.root'] = realpath(__DIR__ . '/../');
$settings['logger.min_level'] = \Psr\Log\LogLevel::CRITICAL;
$settings['logger.filelog.path'] = function($c) {
	return $c['paths.var.log'].'/application.'.$c['app.env'].'.log';
};


$settings['api.credentials'] = function($c) {
	return (new \Loopia\App\Api\Credentials())
			->setUsername($c['api.username'])
			->setPassword($c['api.password']);
};

$settings['api.username'] = 'your.username';
$settings['api.password'] = 'your.password';

$settings['api.endpoint'] = 'http://filmapi.loopiarnd.com';

return $settings;