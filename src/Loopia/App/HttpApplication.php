<?php

/*
 * © Loopia. All rights reserved.
 */

namespace Loopia\App;

use FastRoute\DataGenerator\GroupCountBased as DataGenerator;
use FastRoute\Dispatcher;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std;
use Loopia\App\Error\NotFoundException;
use Loopia\App\Error\MethodNotAllowedException;
use Loopia\App\Error\TemplatePathNotFoundException;
use Loopia\App\Error\TemplatePathNotReadableException;
use Loopia\App\Template\Template;

abstract class HttpApplication extends Application {

	abstract function initRoutes();

	public function __construct(string $env, array $settings) {
		parent::__construct($env, $settings);
		$this->initRouter();
	}

	protected function initRouter() {
		$this->container['route.parser'] = function() {
			return new Std();
		};

		$this->container['route.datagenerator'] = function() {
			return new DataGenerator();
		};

		$this->container['route.collector'] = function($c) {
			$rc = new RouteCollector($c['route.parser'], $c['route.datagenerator']);

			foreach ($this->container['route.routes'] as $route) {
				$rc->addRoute($route[0], $route[1], $route[2]);
			}

			return $rc;
		};

		$this->container['route.dispacher'] = function($c) {
			return new GroupCountBased($c['route.collector']->getData());
		};
	}

	protected function getDispacher(): Dispatcher {
		return $this->container['route.dispacher'];
	}

	protected function renderBody(string $content) {
		$templatePath = $this->container['template.base_path'] . DIRECTORY_SEPARATOR . '_body.phtml';
		return (new Template($templatePath, ['content' => $content]))->render();
	}

	protected function render(string $path, array $data = []) {
		$templatePath = $this->container['template.base_path'] . DIRECTORY_SEPARATOR . $path;

		if (!is_file($templatePath)) {
			throw new TemplatePathNotFoundException($templatePath);
		}

		if (!is_file($templatePath)) {
			throw new TemplatePathNotReadableException($templatePath);
		}

		echo $this->renderBody((new Template($templatePath, $data))->render());
	}

	public function run() {
		$this->initRoutes();

		// Fetch method and URI from somewhere
		$httpMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
		$uriRaw = filter_input(INPUT_SERVER, 'REQUEST_URI');

		// Strip query string (?foo=bar) and decode URI
		if (false !== $pos = strpos($uriRaw, '?')) {
			$uri = substr($uriRaw, 0, $pos);
		}
		$uri = rawurldecode($uriRaw);

		$this->logger->debug('Current route', ['uri' => $uri]);
		$routeInfo = $this->getDispacher()->dispatch($httpMethod, $uri);

		switch ($routeInfo[0]) {
			case Dispatcher::NOT_FOUND:
				$this->logger->info('Route not found', ['uri' => $uri]);
				throw new NotFoundException();

			case Dispatcher::METHOD_NOT_ALLOWED:

				$this->logger->info('Method not allowed', ['method' => $httpMethod, 'uri' => $uri]);
				throw new MethodNotAllowedException($httpMethod);
			case Dispatcher::FOUND:

				$this->logger->info('Route hit', ['method' => $httpMethod, 'uri' => $uri]);
				call_user_func_array($routeInfo[1], $routeInfo[2]);

				break;
		}
	}

}
