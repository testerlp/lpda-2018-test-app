<?php

/*
 * © Loopia. All rights reserved.
 */

namespace Loopia\App;

use Loopia\App\Api\Client;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Pimple\Container;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;

abstract class Application {

	use LoggerAwareTrait;

	/**
	 *
	 * @var Container
	 */
	protected $container;

	public function __construct(string $env, array $settings) {
		$this->container = new Container($settings);
		$this->container['app.env'] = $env;
		$this->initServices();
		$this->initLogger();
	}

	protected function initServices() {

		$this->container['service.fs'] = function() {
			return new Filesystem();
		};

		$this->container['service.api.client'] = $this->container->factory(function($c) {
			return new Client($c['api.credentials'], $c['api.endpoint']);
		});

		$this->container['service.model.storage'] = function() {
			/* @todo */
		};

	}

	protected function initLogger() {

		if (!isset($this->container['paths.var'])) {
			$this->container['paths.var'] = function($c) {
				$dir = $c['paths.root'] . '/var';
				$c['service.fs']->mkdir($dir);

				return $dir;
			};
		}

		if (!isset($this->container['paths.var.log'])) {
			$this->container['paths.var.log'] = function($c) {
				$dir = $c['paths.var'] . '/log';
				$c['service.fs']->mkdir($dir);

				return $dir;
			};
		}

		$this->container['logger'] = function($c) {
			$logger = new Logger('Application');
			$handler = new StreamHandler($this->container['logger.filelog.path'], $this->container['logger.min_level']);
			$logger->pushHandler($handler);

			return $logger;
		};

		$this->setLogger($this->container['logger']);
	}

}
