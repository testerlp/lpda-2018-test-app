<?php

/*
 * © Loopia. All rights reserved.
 */

namespace Loopia\App\Api;

class Credentials {

	private $username;
	private $password;

	public function setUsername(string $username) {
		$this->username = $username;

		return $this;
	}

	public function setPassword(string $password) {
		$this->password = $password;

		return $this;
	}

	public function visit(CredentialsConsumerInterface $cci) {
		return $cci->getConsumerClosure()->call($this);
	}
}
