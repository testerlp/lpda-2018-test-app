<?php

$loader = require_once __DIR__ . '/../vendor/autoload.php';

$env = getenv('APPLICATION_ENV') ?: 'dev';
$confPath = __DIR__ . '/../config/' . $env . '.php';
if (!(is_file($confPath) || is_readable($confPath))) {
	throw new \Exception('Config can\'t be read. Check environment: ' . $env);
}

$config = require $confPath;

return new class($env, $config) extends \Loopia\App\HttpApplication {

	public function initRoutes() {
		$this->container['route.routes'] = function($c) {
			return [
				['GET', '/', function() {
						return $this->render('index.phtml', $this->loadData());
					}],
				['GET', '/items/{id:\d+}', function($id) {
						return $this->render('item.phtml', $this->loadItem($id));
					}],
			];
		};
	}

	protected function loadData(): array {

		/* @var $client \Loopia\App\Api\Client */
		$client = $this->container['service.api.client'];

		/* @var $response \GuzzleHttp\Psr7\Response */
		$response = $client->send($client->getRequest('items'));

		$data = json_decode($response->getBody()->getContents(), true);

		return ['items' => new Doctrine\Common\Collections\ArrayCollection($data)];
	}
};


